set term pdfcairo size 8cm,5cm font "Times,12"
set output 'all.pdf'

set ylabel "Solve time"
set xlabel "Number of actions"

plot \
    for [name in system("ls times.zero.*.txt")] name u 1:2 notitle pt 7 lc rgb "#aa0055aa", \
    for [name in system("ls times.general.*.txt")] name u 1:2 notitle pt 7 lc rgb "#aa55aa00", \
    "<echo 'nan nan'" u 1:2 t 'Zero-sum' pt 7 lc rgb "#0055aa", \
    "<echo 'nan nan'" u 1:2 t 'General-sum' pt 7 lc rgb "#55aa00"