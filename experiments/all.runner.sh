#!/bin/bash
#SBATCH --time=23:00:00
#SBATCH --ntasks=2
#SBATCH --partition=amd
#SBATCH --cpus-per-task=4
#SBATCH --mem-per-cpu=4G

module load Gurobi/11.0.0-GCCcore-12.3.0
module load Julia/1.10.4-linux-x86_64
srun -l --multi-prog experiments/all.conf
