include("../src/CGTGameComplexity.jl")

using Dates

randn_zero_sum_game(n) = randn(n, n)
randn_general_sum_game(n) = (randn(n, n), randn(n, n))

function parse_args(_samples, _end_size, _sum)
    sample_count = parse(Int, _samples)
    end_size = parse(Int, _end_size)

    opts_gen = Dict(
        "zero" => randn_zero_sum_game,
        "general" => randn_general_sum_game
    )

    opts_algo = Dict(
        "zero" => CGTGameComplexity.linear_program,
        "general" => CGTGameComplexity.bilinear_program
    )

    return sample_count, end_size, opts_algo[_sum], opts_gen[_sum]
end

function run_experiment(_samples, _max_size, _sum)
    sample_count, max_size, algo, game_gen = parse_args(_samples, _max_size, _sum)

    date = Dates.format(now(), "yyyy-mm-dd_HH-MM-SS")

    println("# Experiment: $_samples, $_max_size, $_sum, $date")
    open("times.$_sum.$date.txt", "a") do f
        for n in 1:max_size
            for _ in 1:sample_count
                game = game_gen(n)
                try
                    _, _, _, time = algo(game)
                    println(f, "$n $time")
                catch e
                    println(stderr, e)
                end
            end
        end
    end
end

if abspath(PROGRAM_FILE) == @__FILE__
    run_experiment(ARGS...)
end