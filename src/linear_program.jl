using LinearAlgebra
using Gurobi
using JuMP


"""Computes the value and NE strategies for a zero-sum game"""
function linear_program(u::AbstractMatrix; optimizer=Gurobi.Optimizer)
    m = Model(optimizer)

    ny = size(u, 2)
    @variable(m, ys[1:ny], lower_bound=0, upper_bound=1)
    @variable(m, w)

    @constraint(m, sum(ys) == 1)
    @constraint(m, dx, u * ys .<= w)
    @objective(m, Min, w)
    optimize!(m)

    value.(w), dual.(dx), value.(ys), solve_time(m)
end