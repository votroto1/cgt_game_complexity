module CGTGameComplexity

export equilibrium

include("bilinear_program.jl")
include("linear_program.jl")

end
