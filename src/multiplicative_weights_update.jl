using LinearAlgebra
using LoopVectorization

function multiplicative_weights_update(U1::AbstractMatrix{T}, eta::T, iters::Int) where {T}
    s1, s2 = size(U1)

    x1 = ones(T, s1)
    x2 = ones(T, s2)
    sum_x1 = zeros(T, s1)
    sum_x2 = zeros(T, s2)
    v1 = Array{T}(undef, s1)
    v2 = Array{T}(undef, s2)

    for _ in 1:iters
        sum_x1 .+= normalize!(x1, 1)
        sum_x2 .+= normalize!(x2, 1)

        mul!(v1, U1, x2, eta, false)
        mul!(v2, U1', x1, -eta, false)

        @turbo @. x1 *= exp(v1)
        @turbo @. x2 *= exp(v2)
    end

    rdiv!(sum_x1, iters)
    rdiv!(sum_x2, iters)

    return sum_x1, sum_x2
end
