using LinearAlgebra


function fictitious_play(U1::AbstractMatrix{T}, iters::Int) where {T}
    s1, s2 = size(U1)
    U2t = permutedims(U1)
    rmul!(U2t, -1)

    # Matrix offsets to use BLAS.iamax
    umin_1 = minimum(U1)
    umin_2 = minimum(U2t)

    x2 = zeros(T, s2)
    x1 = zeros(T, s1)
    val2 = zeros(T, s2)
    val1 = zeros(T, s1)

    @inbounds for _ in 1:iters
        br1 = BLAS.iamax(val1)
        br2 = BLAS.iamax(val2)
        val1 .+= view(U1, :, br2) .- umin_1
        val2 .+= view(U2t, :, br1) .- umin_2
        x1[br1] += 1
        x2[br2] += 1
    end

    rdiv!(x2, iters)
    rdiv!(x1, iters)

    return x1, x2
end