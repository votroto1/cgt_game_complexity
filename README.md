# Experiments with fundamental algorithms to solve strategic games

Experiment which establishes the runtime required to solve zero-sum and general-sum games depending on their size.

# Install

 1. Download the git project;
    ```sh
    git clone --depth 1 https://gitlab.fel.cvut.cz/votroto1/cgt_game_complexity.git
    ```
 2. Instantiate the julia project.
    ```sh
    srun --mem-per-cpu=16GB -p amdfast -n1 -c1 --pty bash -i
    ml Julia
    ml Gurobi
    ] instantiate
    ```
# Usage

Use the Slurm batch script,
```sh
sbatch experiments/all.runner.sh
```

or run the experiment interactively.
```sh
# Make sure the number of threads in Gurobi and cpus in slurm makes sense.
srun --mem-per-cpu=4GB -p amdfast -n1 -c4 --pty bash -i

ml Gurobi
ml Julia

# The args are: sample_count max_size (zero|general)
julia --project experiments/run_experiment.jl 5 100 zero
```
Plot the results.
```sh
gnuplot plotting/plot.all.gnu
```